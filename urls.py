from django.urls import path

from . import views
from . import api

app_name = "co2app"

urlpatterns = [
    path("", views.index, name="index"),
    path("detail/<slug:hostname>/", views.detail),
    path("api/getroom/", api.getroom),
    path("api/setroom/", api.setroom),
    path("api/calibrate/", api.calibrate),
    path("api/getlamp/", api.getlamp)
]
