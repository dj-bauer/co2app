from django.shortcuts import get_object_or_404
from django.utils import timezone
from rest_framework.decorators import api_view
from rest_framework.response import Response
import socket

from .apps import Co2AppConfig
from .models import Sensor, LogEntry

@api_view(["POST"])
def getroom(request):
    if request.data.get("hostname") == None:
        return Response("Please add hostname to post")
    s = get_object_or_404(Sensor, hostname=request.data.get("hostname"))
    return Response({"hostname": s.hostname, "value": s.current_value, "last_log": s.last_log})

@api_view(["POST"])
def setroom(request):
    ip = request.META["REMOTE_ADDR"]
    
    if request.data.get("data") == None:
        txt = "[Error] post entry data is missing"
        print(txt)
        return Response(txt)
    if request.data.get("hostname") != None:
        hostname = request.data.get("hostname")
    else:
        try:
            response = socket.gethostbyaddr(ip)
            hostname = response[0].split(".")[0]
        except socket.herror:
            return Response("Cannot resolv ip address")
    # Nicht konfigurierte Sensoren ignorieren.
    if hostname.startswith("ESP-"):
            return Response("This esp has no dns name yet")
    
    value = int(request.data.get("data"))
    
    s = Sensor.objects.filter(hostname=hostname)
    if not s.exists():
        Sensor.init(hostname, value)
    else:
        print("[Logged] {}: {}".format(hostname, value))
        s.first().log(value)

    return Response({"code": 200})

@api_view(["POST"])
def calibrate(request):
    result = "-1"
    if request.data.get("treshold") != None:
        treshold = int(request.data.get("treshold"))   
        if treshold == 0:
            result = Co2AppConfig.treshold_trigger
        if treshold == 1:
            result = Co2AppConfig.treshold_critical
    return Response(result)

def is_on_time():
    now = timezone.now().time()
    return Co2AppConfig.lamp_automatic_start <= now <= Co2AppConfig.lamp_automatic_end 

@api_view(["GET"])
def getlamp(request):
    if Co2AppConfig.lamp_ismanual:
        if Co2AppConfig.lamp_manual:
            r = "1"
        else:
            r = "0"
    else:
        if is_on_time():
            r = "1"
        else:
            r = "0"
    return Response(r)
