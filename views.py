from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .apps import Co2AppConfig
from .models import Sensor, LogEntry
from django.utils import timezone

# Create your views here.

def index(request):
    sensors = Sensor.objects.all().order_by("-last_log")
    c = Co2AppConfig
    d = {
            "sensors": sensors,
            "treshold_critical": c.treshold_critical,
            "treshold_trigger": c.treshold_trigger,
            "color_critical": c.color_critical,
            "color_trigger": c.color_trigger,
        }
    return render(request, "co2app/index.html", d)

def detail(request, hostname : str):
   s = get_object_or_404(Sensor, hostname=hostname)
   twentiefour_h_ago = timezone.now()-timezone.timedelta(hours=24)
   data = LogEntry.objects.filter(sensor=s).filter(timestamp__gt=twentiefour_h_ago)
   # Limit it to the last 1000 entries to prevent some overflow of the graph or something
   count = data.count()
   if(count > 1000):
       data = data[count-1000:]
   d = {
            "sensor": s,
            "history": data,
            "treshold_critical": Co2AppConfig.treshold_critical,
            "treshold_trigger": Co2AppConfig.treshold_trigger,
            "color_critical": Co2AppConfig.color_critical,
            "color_trigger": Co2AppConfig.color_trigger,
        }
   return render(request, "co2app/detail.html", d)
