from django.db import models
from django.utils import timezone

class Sensor(models.Model):
    hostname = models.CharField(max_length=40, unique=True)
    last_log = models.DateTimeField("last log timestamp")
    current_value = models.IntegerField(default=-1)

    def init(host : str, v : int):
        d = timezone.now()
        s = Sensor.objects.create(hostname=host, last_log=d, current_value=v)
        s.log(v)

    def log(self, v : int):
        d = timezone.now()
        LogEntry.objects.create(sensor=self, value=v, timestamp=d)
        self.last_log = d
        self.current_value = v
        self.save()

class LogEntry(models.Model):
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    value = models.IntegerField(default=-1)
    timestamp = models.DateTimeField("timestamp")
            

# Create your models here.
