from django.apps import AppConfig
from datetime import time

class Co2AppConfig(AppConfig):
    name = 'co2app'

    treshold_trigger = 800
    treshold_critical = 1000

    color_trigger = "#fcd303"
    color_critical = "#fc3503"

    lamp_ismanual = False
    lamp_manual = True
    lamp_automatic_start = time(hour=8)
    lamp_automatic_end = time(hour = 22)
