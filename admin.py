from django.contrib import admin
from .models import  Sensor, LogEntry
# Register your models here.
admin.site.register(Sensor)
admin.site.register(LogEntry)
